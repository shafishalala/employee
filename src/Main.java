public class Main {
    public static void main(String[] args) {
        Manager manager = new Manager("Asad Asasdf", 7000.0, "Marketing");
        manager.getDetails();

        System.out.println();

        Developer developer = new Developer("Shalala", 4500.0, "Java");
        developer.getDetails();
    }
}